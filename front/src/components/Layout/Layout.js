import React from 'react';
import {Button, Layout as AntLayout, Space} from "antd";
import {Link} from "react-router-dom";
import usersSlice from "../../store/slices/usersSlice";
import {useDispatch, useSelector} from "react-redux";
import {historyPush} from "../../store/actions/historyActions";

const {Header} = AntLayout;

const {logoutRequest} = usersSlice.actions;

const Layout = ({children}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const logout = () => {
    dispatch(logoutRequest());
  };

  const push = path => {
    dispatch(historyPush(path));
  }

  return (
    <AntLayout>
      <Header style={{color: '#fff', display: 'flex', justifyContent: 'space-between', padding: '0 100px', marginBottom: 20}}>
        <div>
          <Link to='/' style={{textTransform: "uppercase", color: "#fff", fontWeight: 800}}>
            Photo Gallery
          </Link>
        </div>
        {user ?
            <Space size={20}>
              <span>Hello {user.displayName}</span>
              <Button type='primary' onClick={logout}>Logout</Button>
            </Space> :
            <Space>
              <Button type='primary' onClick={() => push('/login')}>Login</Button>
              <Button type='primary' onClick={() => push('/register')}>Register</Button>
            </Space>
        }
      </Header>
      {children}
    </AntLayout>
  );
};

export default Layout;

import React, {useState} from 'react';
import {Button, Card as AntCard, Modal, Space} from 'antd';
import noImage from '../../assets/images/no-image.jpg';
import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import photoSlices from "../../store/slices/photoSlices";

const {deleteRequest} = photoSlices.actions;

const Card = ({title, image, id, ownUser}) => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.photo.createLoading);
  const user = useSelector(state => state.users.user);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const onDelete = () => {
    dispatch(deleteRequest(id));
  };


  let cardImage = noImage;

  if (image) {
    cardImage = apiURL + '/' + image;
  }



  return (
    <AntCard
      hoverable
      style={{ width: 240 }}
      cover={<img alt={title} src={cardImage} onClick={showModal} />}
      bodyStyle={{backgroundColor: '#ececec'}}
    >
      <Space direction="vertical" size={10} align='center' style={{width: '100%'}}>
        <div><Button type='link' onClick={showModal}>{title}</Button></div>
        {ownUser.displayName ? <div><Link to={'/user/' + ownUser?._id}>By {ownUser?.displayName}</Link></div> : null}
        {user?._id === ownUser._id ? <div><Button danger loading={loading} type="link" onClick={onDelete}>Delete</Button></div> : null}
      </Space>

      <Modal title={title} visible={isModalVisible} width={700} onOk={() => setIsModalVisible(false)} onCancel={() => setIsModalVisible(false)} centered footer={[]}>
        <img alt={title} src={cardImage} width={600} height='auto' style={{display: 'block', marginInline: 'auto'}}/>
      </Modal>
    </AntCard>
  );
};

export default Card;

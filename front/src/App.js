import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import Main from "./containers/Main/Main";
import Register from "./containers/Register | Login/Register";
import Login from "./containers/Register | Login/Login";
import Layout from "./components/Layout/Layout";
import {Content} from "antd/es/layout/layout";
import AddPhoto from "./containers/AddPhoto/AddPhoto";
import User from "./containers/User/User";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);
  return (
    <Layout>
      <Content style={{ padding: '0 50px', backgroundColor: '#fff' }}>
        <Switch>
          <Route path='/' exact component={Main} />
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
          <ProtectedRoute
            path="/addPhoto"
            exact
            component={AddPhoto}
            isAllowed={user}
            redirectTo="/login"
          />
          <ProtectedRoute
            path="/user/:id"
            component={User}
            isAllowed={user}
            redirectTo="/login"
          />
        </Switch>
      </Content>
    </Layout>
  );
};

export default App;

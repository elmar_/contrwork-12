import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import photoSlice from "./slices/photoSlices";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  photo: photoSlice.reducer
});

export default rootReducer;

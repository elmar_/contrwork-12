import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  createLoading: false,
  createError: null,
  photos: [],
  photo: null,
  fetchPhotosLoading: false,
  fetchPhotosError: null,
  deleteLoading: false,
  deleteError: null
};

const name = 'photos';

const photoSlice = createSlice({
  name,
  initialState,
  reducers: {
    createPhotoRequest: state => {
      state.createLoading = true;
    },
    createPhotoSuccess: state => {
      state.createLoading = false;
      state.createError = null;
    },
    createPhotoError: (state, {payload: error}) => {
      state.createLoading = false;
      state.createError = error;
    },
    fetchRequest: state => {
      state.fetchPhotosLoading = true;
    },
    fetchSuccess: (state, {payload: photos}) => {
      state.fetchPhotosLoading = false;
      state.fetchPhotosError = null;
      state.photos = photos;
    },
    fetchError: (state, {payload: error}) => {
      state.fetchPhotosLoading = false;
      state.fetchPhotosError = error;
    },
    deleteRequest: state => {
      state.deleteLoading = true;
    },
    deleteSuccess: (state, {payload: id}) => {
      const index = state.photos.findIndex(item => item._id === id);
      state.photos.splice(index, 1);
      state.deleteLoading = false;
    },
    deleteFailure: (state, {payload: error}) => {
      state.deleteLoading = false;
      state.deleteError = error;
    }

  }
});

export default photoSlice;

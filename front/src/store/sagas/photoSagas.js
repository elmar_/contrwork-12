import {put, takeEvery} from "redux-saga/effects";
import {historyPush} from "../actions/historyActions";
import axiosApi from "../../axiosApi";
import {notificationError, notificationSuccess} from "../../components/Notification/Notification";
import photoSlice from "../slices/photoSlices";

const {
  createPhotoError,
  createPhotoRequest,
  createPhotoSuccess,
  fetchRequest,
  fetchSuccess,
  fetchError,
  deleteRequest,
  deleteSuccess,
  deleteFailure,

} = photoSlice.actions;


export function* createPhoto({payload: photoData}) {
  try {
    const data = new FormData();
    Object.keys(photoData).forEach(key => {
      data.append(key, photoData[key]);
    });

    yield axiosApi.post('/photos', data);
    yield put(createPhotoSuccess());
    yield put(historyPush('/'));
    notificationSuccess('Photo was created');
  } catch (error) {
    notificationError('Photo was not created');
    yield put(createPhotoError(error));
  }
}

export function* fetchPhotos({payload: user}) {
  try {

    const response = yield axiosApi.get('/photos', {params: {user}});
    yield put(fetchSuccess(response.data));
  } catch (error) {
    notificationError('Photos was not fetched');
    yield put(fetchError(error));
  }
}

function* deletePhoto({payload: id}) {
  try{
    yield axiosApi.delete('/photos/' + id);
    yield put(deleteSuccess(id));
    notificationSuccess('Photo was deleted');
  } catch (e) {
    console.error(e);
    notificationError('Cant delete cocktail');
    yield put(deleteFailure(e));
  }
}


const usersSagas = [
  takeEvery(createPhotoRequest, createPhoto),
  takeEvery(fetchRequest, fetchPhotos),
  takeEvery(deleteRequest, deletePhoto),
];

export default usersSagas;

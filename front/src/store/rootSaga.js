import {all} from "redux-saga/effects";
import historySagas from "./sagas/historySagas";
import history from "../history";
import usersSagas from "./sagas/usersSagas";
import photoSagas from "./sagas/photoSagas";


function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...usersSagas,
    ...photoSagas
  ]);
}

export default rootSaga;

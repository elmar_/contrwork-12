import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Empty, Row, Space, Typography} from "antd";
import {Link} from "react-router-dom";
import photoSlice from "../../store/slices/photoSlices";
import Card from "../../components/Card/Card";
import Spinner from "../../components/Spinner/Spinner";

const {fetchRequest} = photoSlice.actions;

const Main = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const photos = useSelector(state => state.photo.photos);
  const loading = useSelector(state => state.photo.fetchPhotosLoading);
  const error = useSelector(state => state.photo.fetchPhotosError);

  useEffect(() => {
    dispatch(fetchRequest());
  }, [dispatch]);

  let content = (
    <Space direction="horizontal" size={20} wrap={true}>
      {photos.map(item => (
        <Card title={item.title} image={item.image} key={item._id} id={item._id} ownUser={item.user}/>
      ))}
    </Space>
  );

  if  (photos.length === 0) {
    content = <Empty description={error || "No photos"} />
  }

  if (loading) {
    content = <Spinner />
  }

  return (
    <>
      <Row justify="space-between" style={{marginBottom: 30}}>
        <Col span={6} style={{textAlign: 'center'}}>
          <Typography.Title level={5}>All photos</Typography.Title>
        </Col>

        {user ?
          <Col span={6} style={{textAlign: 'center'}}>
            <Link component={Button} to="/addPhoto">Create cocktail</Link>
          </Col> : null}
      </Row>

      {content}
    </>
  );
};

export default Main;

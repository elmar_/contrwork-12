import React from 'react';
import {Alert, Button, Col, Form, Input, Row, Upload} from "antd";
import {UploadOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import photoSlice from "../../store/slices/photoSlices";

const {createPhotoRequest} = photoSlice.actions;

const AddPhoto = () => {
  const loading = useSelector(state => state.photo.createLoading);
  const error = useSelector(state => state.photo.createError);
  const dispatch = useDispatch();

  const onFinish = data => {
    if (data.image?.length){
      const newData = {...data, image: data.image[0].originFileObj}
      dispatch(createPhotoRequest(newData));
    } else {
      console.error('input image');
    }
  };

  const normFile = (e) => {

    if (Array.isArray(e)) {
      return e;
    }

    return e && e.fileList;
  };

  return (
    <div>
      Add Photo
      <Form
        onFinish={onFinish}
      >
        {<Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              {error && <Alert
                message="Error Text"
                description={error.message || error.global || "Error, try again"}
                type="error"
              />}
            </Form.Item>
          </Col>
        </Row>}

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Title"
              name="title"
              rules={[
                {
                  required: true,
                  message: 'Please input your email!',
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              name="image"
              label="Photo"
              valuePropName="fileList"
              getValueFromEvent={normFile}
              rules={[
                {
                  required: true,
                  message: 'Please input photo!'
                },
              ]}
            >
              <Upload name="logo" listType="picture" maxCount={1} beforeUpload={() => false}>
                <Button icon={<UploadOutlined />}>Click to upload</Button>
              </Upload>
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9}>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading} style={{marginRight: 20}}>
                Add Photo
              </Button>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </div>
  );
};

export default AddPhoto;

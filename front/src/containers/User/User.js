import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Row, Col, Empty, Space, Typography} from "antd";
import Card from "../../components/Card/Card";
import Spinner from "../../components/Spinner/Spinner";
import photoSlice from "../../store/slices/photoSlices";
import usersSlice from "../../store/slices/usersSlice";

const {fetchRequest} = photoSlice.actions;
const {oneRequest} = usersSlice.actions;

const User = ({match}) => {
  const dispatch = useDispatch();
  const photos = useSelector(state => state.photo.photos);
  const loading = useSelector(state => state.photo.fetchPhotosLoading);
  const error = useSelector(state => state.photo.fetchPhotosError);
  const supUser = useSelector(state => state.users.supUser);

  const id = match.params.id;

  useEffect(() => {
    dispatch(oneRequest(id));
    dispatch(fetchRequest(id));
  }, [dispatch, id]);

  let content = (
    <Space direction="horizontal" size={20} wrap={true}>
      {photos.map(item => (
        <Card title={item.title} image={item.image} key={item._id} id={item._id} ownUser={item.user}/>
      ))}
    </Space>
  );

  if  (photos.length === 0) {
    content = <Empty description={error || "No photos"} />
  }

  if (loading) {
    content = <Spinner />
  }

  return (
    <>
      <Row justify="space-between" style={{marginBottom: 30}}>
        <Col span={6} style={{textAlign: 'center'}}>
          <Typography.Title level={5}>By {supUser?.displayName}</Typography.Title>
        </Col>
      </Row>

      {content}
    </>
  );
};

export default User;

const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, user2] = await User.create({
    email: 'user@test.com',
    password: '123',
    token: nanoid(),
    displayName: 'user',
  }, {
    email: 'user2@test.com',
    password: '123',
    token: nanoid(),
    displayName: 'user2'
  });

  await Photo.create({
    title: 'Adele 1',
    image: 'fixtures/adele_track.jpg',
    user: user
  },{
    title: 'Adele 2',
    image: 'fixtures/adele_track2.jpg',
    user: user2
  });





  await mongoose.connection.close();
};

run().catch(e => console.error(e));

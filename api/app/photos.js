const express = require('express');
const Photo = require("../models/Photo");
const auth = require("../middleware/auth");
const upload = require('../multer').photos;

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const user = req.query.user;
    if (user) {
      const photos = await Photo.find({user});
      return res.send(photos);
    } else {
      const photos = await Photo.find().populate('user', 'displayName');
      return res.send(photos);
    }

  }catch (e) {
    console.error(e);
    return res.status(500).send({message: 'error in get photos'});
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).send({message: 'You forgot to send photo'});
    }

    const photo = new Photo({
      title: req.body.title,
      user: req.user._id,
      image: req.file.filename
    });

    await photo.save();

    return res.send(photo);
  }catch (e) {
    console.error(e);
    return res.status(500).send({message: 'error in post photos'});
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const photo = await Photo.findOne({_id: req.params.id});

    if (req.user._id.toString() !== photo.user.toString()) {
      return res.status(401).send({message: 'not yours'});
    }

    await Photo.deleteOne({_id: req.params.id});


    return res.send(photo);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in delete cocktails'});
  }
});

module.exports = router;
